####
# Defaults
####

locals {
  should_fetch_default_subnet         = local.use_default_subnets
  should_fetch_default_security_group = length(var.vpc_security_group_ids) == 0
  should_fetch_default_vpc            = local.should_fetch_default_subnet || local.should_fetch_default_security_group
  should_fetch_default_ami            = var.ami == ""

  current_region     = var.current_region != "" ? var.current_region : data.aws_region.this["0"].name
  current_account_id = var.current_account_id != "" ? var.current_account_id : data.aws_caller_identity.this["0"].account_id
}

data "aws_caller_identity" "this" {
  for_each = var.current_account_id == "" ? { 0 = "enabled" } : {}
}

data "aws_region" "this" {
  for_each = var.current_region == "" ? { 0 = "enabled" } : {}
}

data "aws_ec2_instance_type_offerings" "this" {
  for_each = local.should_fetch_default_subnet ? { 0 = 0 } : {}

  filter {
    name   = "instance-type"
    values = [var.instance_type]
  }

  location_type = "availability-zone-id"
}

data "aws_availability_zones" "default" {
  count = local.should_fetch_default_subnet ? 1 : 0

  state = "available"

  filter {
    name   = "zone-id"
    values = distinct(data.aws_ec2_instance_type_offerings.this["0"].locations)
  }
}

data "aws_vpc" "default" {
  count = local.should_fetch_default_vpc ? 1 : 0

  default = true
}

data "aws_subnets" "default" {
  count = local.should_fetch_default_subnet ? length(data.aws_availability_zones.default[0].names) : 0

  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default[0].id]
  }

  filter {
    name   = "availability-zone"
    values = [element(data.aws_availability_zones.default[0].names, count.index)]
  }
}

data "aws_security_group" "default" {
  count = local.should_fetch_default_security_group ? 1 : 0

  vpc_id = data.aws_vpc.default[0].id
  name   = "default"
}

####
# Subnets
####

data "aws_subnet" "current" {
  count = length(local.subnet_ids)

  id = local.subnet_ids[count.index]
}

####
# IAM Instance Profile
####

data "aws_iam_policy_document" "sts_instance" {
  count = local.should_create_instance_profile && local.should_create_iam_instance_profile_iam_role ? 1 : 0

  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "ec2.amazonaws.com"
      ]
    }
  }
}

####
# SSM Parameter
####

data "aws_ssm_parameter" "default_ami" {
  count = local.should_fetch_default_ami ? 1 : 0

  name = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-ebs"
}
