####
# EC2
####

locals {
  is_t_instance_type = replace(var.instance_type, "/^t[23]{1}\\..*$/", "1") == "1" ? "1" : "0"
}

// As of 2024-07-04 we can't make conditional lifecycle
// aws_instance.this and aws_instance.this_ignore_changes are the same except lifecycle
// Don't forget to update both at the same time when doing modifications
resource "aws_instance" "this" {
  for_each = !var.use_autoscaling_group && !var.ec2_ignore_change_on_ami_and_user_data ? { 0 = "enabled" } : {}

  ami                         = local.ami
  instance_type               = var.instance_type
  user_data                   = var.user_data
  user_data_replace_on_change = var.ec2_user_data_replace_on_change
  key_name                    = local.key_pair_name
  monitoring                  = var.monitoring
  host_id                     = var.host_id

  cpu_core_count       = var.cpu_core_count
  cpu_threads_per_core = var.cpu_threads_per_core

  network_interface {
    device_index         = 0
    network_interface_id = local.primary_eni_id
  }

  iam_instance_profile = local.iam_instance_profile_name

  ebs_optimized = var.ebs_optimized
  volume_tags = merge(
    local.tags,
    var.volume_tags,
  )

  dynamic "root_block_device" {
    for_each = local.should_update_root_device ? [1] : []

    content {
      delete_on_termination = var.root_block_device_delete_on_termination
      encrypted             = var.root_block_device_encrypted
      iops                  = var.root_block_device_iops
      throughput            = var.root_block_device_throughput
      volume_size           = var.root_block_device_volume_size
      volume_type           = var.root_block_device_volume_type
      kms_key_id            = local.volume_kms_key_arn
    }
  }

  dynamic "ephemeral_block_device" {
    for_each = var.ephemeral_block_devices

    content {
      device_name  = ephemeral_block_device.value.device_name
      no_device    = lookup(ephemeral_block_device.value, "no_device", null)
      virtual_name = lookup(ephemeral_block_device.value, "virtual_name", null)
    }
  }

  disable_api_termination              = var.disable_api_termination
  instance_initiated_shutdown_behavior = var.instance_initiated_shutdown_behavior
  placement_group                      = var.placement_group
  tenancy                              = var.tenancy

  dynamic "credit_specification" {
    for_each = local.is_t_instance_type && var.cpu_credits != "" ? [1] : []

    content {
      cpu_credits = var.cpu_credits
    }
  }

  dynamic "metadata_options" {
    for_each = var.metadata_options_http_endpoint_enabled ? [1] : []

    content {
      http_endpoint               = "enabled"
      http_tokens                 = var.metadata_options_http_tokens_required == true ? "required" : "optional"
      http_put_response_hop_limit = var.metadata_options_http_put_response_hop_limit
      instance_metadata_tags      = var.metadata_options_instance_metadata_tags_enabled == true ? "enabled" : "disabled"
    }
  }

  dynamic "metadata_options" {
    for_each = !var.metadata_options_http_endpoint_enabled ? [1] : []

    content {
      http_endpoint = "disabled"
    }
  }

  tags = merge(
    local.tags,
    {
      Name = format("%s%s", var.prefix, var.name)
    },
    var.instance_tags,
  )

  lifecycle {
    ignore_changes = [
      private_ip,
      root_block_device,
      volume_tags,
    ]
  }
}

resource "aws_instance" "this_ignore_ami_user_data_changes" {
  for_each = !var.use_autoscaling_group && var.ec2_ignore_change_on_ami_and_user_data ? { 0 = "enabled" } : {}

  ami                         = local.ami
  instance_type               = var.instance_type
  user_data                   = var.user_data
  user_data_replace_on_change = var.ec2_user_data_replace_on_change
  key_name                    = local.key_pair_name
  monitoring                  = var.monitoring
  host_id                     = var.host_id

  dynamic "cpu_options" {
    for_each = var.cpu_core_count != null ? [1] : []

    content {
      core_count       = var.cpu_core_count
      threads_per_core = var.cpu_threads_per_core
    }
  }

  network_interface {
    device_index         = 0
    network_interface_id = local.primary_eni_id
  }

  iam_instance_profile = local.iam_instance_profile_name

  ebs_optimized = var.ebs_optimized
  volume_tags = merge(
    local.tags,
    var.volume_tags,
  )

  dynamic "root_block_device" {
    for_each = local.should_update_root_device ? [1] : []

    content {
      delete_on_termination = var.root_block_device_delete_on_termination
      encrypted             = var.root_block_device_encrypted
      iops                  = var.root_block_device_iops
      throughput            = var.root_block_device_throughput
      volume_size           = var.root_block_device_volume_size
      volume_type           = var.root_block_device_volume_type
      kms_key_id            = local.volume_kms_key_arn
    }
  }

  dynamic "ephemeral_block_device" {
    for_each = var.ephemeral_block_devices

    content {
      device_name  = ephemeral_block_device.value.device_name
      no_device    = lookup(ephemeral_block_device.value, "no_device", null)
      virtual_name = lookup(ephemeral_block_device.value, "virtual_name", null)
    }
  }

  disable_api_termination              = var.disable_api_termination
  instance_initiated_shutdown_behavior = var.instance_initiated_shutdown_behavior
  placement_group                      = var.placement_group
  tenancy                              = var.tenancy

  dynamic "credit_specification" {
    for_each = local.is_t_instance_type && var.cpu_credits != "" ? [1] : []

    content {
      cpu_credits = var.cpu_credits
    }
  }

  dynamic "metadata_options" {
    for_each = var.metadata_options_http_endpoint_enabled == true ? [1] : []

    content {
      http_endpoint               = "enabled"
      http_tokens                 = var.metadata_options_http_tokens_required == true ? "required" : "optional"
      http_put_response_hop_limit = var.metadata_options_http_put_response_hop_limit
      instance_metadata_tags      = var.metadata_options_instance_metadata_tags_enabled == true ? "enabled" : "disabled"
    }
  }

  dynamic "metadata_options" {
    for_each = var.metadata_options_http_endpoint_enabled == false ? [1] : []

    content {
      http_endpoint = "disabled"
    }
  }

  tags = merge(
    local.tags,
    {
      Name = format("%s%s", var.prefix, var.name)
    },
    var.instance_tags,
  )

  lifecycle {
    ignore_changes = [
      private_ip,
      root_block_device,
      volume_tags,
      user_data,
      ami
    ]
  }
}

locals {
  should_create_primary_eni = var.use_autoscaling_group == false && var.ec2_primary_network_interface_create

  primary_eni_id = local.should_create_primary_eni ? aws_network_interface.this_primary[0].id : var.ec2_external_primary_network_interface_id
}

resource "aws_network_interface" "this_primary" {
  for_each = local.should_create_primary_eni ? { 0 = "enabled" } : {}

  description     = format("%s%s", var.prefix, "${var.name} root network interface")
  subnet_id       = local.subnet_ids[0]
  security_groups = local.security_group_ids

  private_ips_count = var.ipv4_address_count
  private_ips       = concat(var.ec2_ipv6_addresses, var.ec2_ipv4_addresses)

  source_dest_check = var.ec2_source_dest_check

  tags = merge(
    local.tags,
    var.network_interface_tags,
  )

  lifecycle {
    ignore_changes = [
      private_ips,
      private_ips_count
    ]
  }
}
