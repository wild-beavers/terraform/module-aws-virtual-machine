output "aws_launch_template" {
  value = var.use_autoscaling_group ? { for k, v in aws_launch_template.this["0"] :
    k => v if !contains(["tags", "tag_specifications", "user_data", "image_id", "network_interfaces", "security_group_names", "vpc_security_group_ids"], k)
  } : null
}

output "aws_autoscaling_group" {
  value = var.use_autoscaling_group ? { for k, v in aws_autoscaling_group.this["0"] :
    k => v if !contains(["name_prefix", "tags", "tag", "suspended_processes", "instance_refresh", "enabled_metrics", "target_group_arns", "traffic_source"], k)
  } : null
}

output "aws_autoscaling_schedules" {
  value = var.use_autoscaling_group && length(var.autoscaling_schedules) > 0 ? { for k, v in aws_autoscaling_schedule.this :
    k => { for i, j in v : i => j if !contains(["start_time"], i) }
  } : null
}

output "aws_iam_service_linked_role" {
  value = var.use_autoscaling_group ? { for k, v in aws_iam_service_linked_role.asg["0"] :
    k => v if !contains(["tags"], k)
  } : null
}

output "aws_instance" {
  value = !var.use_autoscaling_group ? { for k, v in merge(aws_instance.this, aws_instance.this_ignore_ami_user_data_changes)["0"] :
    k => v if !contains(["tags", "get_password_data", "user_data", "root_block_device", "volume_tags", "ebs_block_device"], k)
  } : null
}

output "aws_network_interface" {
  value = local.should_create_primary_eni ? { for k, v in aws_network_interface.this_primary["0"] :
    k => v if !contains(["tags", "attachment"], k)
  } : null
}

output "aws_iam_instance_profile" {
  value = local.should_create_instance_profile ? { for k, v in aws_iam_instance_profile.this["0"] :
    k => v if !contains(["tags"], k)
  } : null
}

output "aws_iam_role" {
  value = local.should_create_instance_profile && local.should_create_iam_instance_profile_iam_role ? { for k, v in aws_iam_role.this_instance_profile["0"] :
    k => v if !contains(["tags", "assume_role_policy", "inline_policy", "managed_policy_arns"], k)
  } : null
}

output "aws_key_pair" {
  value = var.key_pair_create ? { for k, v in aws_key_pair.this["0"] :
    k => v if !contains(["tags"], k)
  } : null
}

output "aws_kms_key" {
  value = local.should_create_kms_key ? { for k, v in aws_kms_key.this_volume["0"] :
    k => v if !contains(["tags"], k)
  } : null
}

output "aws_eips" {
  value = local.should_create_primary_eip || local.should_create_eip_for_extra_network_interfaces ? merge(
    local.should_create_primary_eip ? {
      primary = { for k, v in aws_eip.this_primary["0"] : k => v if !contains(["tags", "association_id", "instance", "private_dns", "private_ip", "network_interface"], k) }
    } : {},
    local.should_create_eip_for_extra_network_interfaces ? { for k, v in aws_eip.this_extra :
      k => { for i, j in v : i => j if !contains(["tags", "association_id", "instance", "private_dns", "private_ip", "network_interface"], i) }
    } : {},
  ) : null
}

output "aws_network_interfaces" {
  value = local.should_create_primary_eni || local.should_create_extra_network_interface ? merge(
    local.should_create_primary_eni ? {
      primary = { for k, v in aws_network_interface.this_primary["0"] : k => v if !contains(["tags", "attachment", "security_groups"], k) }
    } : {},
    local.should_create_extra_network_interface ? { for k, v in aws_network_interface.this_extra :
      k => { for i, j in v : i => j if !contains(["tags", "attachment", "security_groups"], i) }
    } : {},
  ) : null
}

output "aws_ebs_volumes" {
  value = local.should_create_extra_volumes ? { for k, v in aws_ebs_volume.this_extra :
    k => { for i, j in v : i => j if !contains(["tags"], i) }
  } : null
}

output "precomputed" {
  value = {
    aws_kms_key = local.should_create_kms_key ? {
      name = format("%s%s", var.prefix, var.volume_kms_key_name)
      arn  = provider::aws::arn_build("aws", "kms", local.current_region, local.current_account_id, format("%s/%s%s", "alias", var.prefix, var.volume_kms_key_name))
    } : null
    aws_iam_role = local.should_create_instance_profile && local.should_create_iam_instance_profile_iam_role ? {
      name = var.iam_instance_profile_iam_role_name != null ? format("%s%s", var.prefix, var.iam_instance_profile_iam_role_name) : null
      arn  = var.iam_instance_profile_iam_role_name != null ? provider::aws::arn_build("aws", "iam", "", local.current_account_id, format("%s%s%s%s", "role", var.iam_instance_profile_path, var.prefix, var.iam_instance_profile_iam_role_name)) : null
      path = var.iam_instance_profile_path
    } : null
  }
}
