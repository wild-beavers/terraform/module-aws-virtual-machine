locals {
  should_update_root_device = var.root_block_device_volume_type != "" || var.root_block_device_volume_size != "" || var.root_block_device_encrypted == true || var.root_block_device_iops != null

  use_default_subnets = var.use_default_subnet == true ? true : (
    (
      var.use_default_subnet == false ? false : (
        var.use_autoscaling_group ? length(var.autoscaling_group_subnet_ids) == 0 : var.ec2_subnet_id == null
      )
    )
  )

  subnet_ids         = var.use_autoscaling_group ? (local.use_default_subnets ? flatten(data.aws_subnets.default[*].ids) : var.autoscaling_group_subnet_ids) : (local.use_default_subnets ? [flatten(data.aws_subnets.default[*].ids)[0]] : [var.ec2_subnet_id])
  availability_zones = data.aws_subnet.current[*].availability_zone

  security_group_ids = local.should_fetch_default_security_group ? data.aws_security_group.default[*].id : var.vpc_security_group_ids

  ami = nonsensitive(local.should_fetch_default_ami ? concat(data.aws_ssm_parameter.default_ami[*].value, [""])[0] : var.ami)

  extra_volume_count = length(var.extra_volumes)

  tags = merge(
    var.tags,
    {
      managed-by = "Terraform"
      origin     = "gitlab.com/wild-beavers/terraform/module-aws-virtual-machine"
    },
  )
}

####
# Instance Profile
####

locals {
  should_create_instance_profile              = var.iam_instance_profile_create == true
  should_create_iam_instance_profile_iam_role = var.iam_instance_profile_iam_role_create == true

  iam_instance_profile_name = local.should_create_instance_profile ? aws_iam_instance_profile.this["0"].name : var.iam_instance_profile_name
}

resource "aws_iam_instance_profile" "this" {
  for_each = local.should_create_instance_profile ? { 0 = "enabled" } : {}

  name = format("%s%s", var.prefix, var.iam_instance_profile_name)
  path = var.iam_instance_profile_path

  role = local.should_create_iam_instance_profile_iam_role ? aws_iam_role.this_instance_profile["0"].id : var.iam_instance_profile_iam_role_id

  tags = merge(
    local.tags,
    var.iam_instance_profile_iam_instance_profile_tags,
  )
}

resource "aws_iam_role" "this_instance_profile" {
  for_each = local.should_create_instance_profile && local.should_create_iam_instance_profile_iam_role ? { 0 = "enabled" } : {}

  name               = var.iam_instance_profile_iam_role_name != null ? format("%s%s", var.prefix, var.iam_instance_profile_iam_role_name) : null
  description        = var.iam_instance_profile_iam_role_description
  path               = var.iam_instance_profile_path
  assume_role_policy = data.aws_iam_policy_document.sts_instance[0].json

  tags = merge(
    local.tags,
    var.iam_instance_profile_iam_role_tags,
  )
}

resource "aws_iam_role_policy_attachment" "this_instance_profile" {
  for_each = local.should_create_instance_profile ? var.iam_instance_profile_iam_role_policy_arns : {}

  role       = local.should_create_iam_instance_profile_iam_role ? aws_iam_role.this_instance_profile["0"].id : var.iam_instance_profile_iam_role_id
  policy_arn = each.value
}

####
# Elastic IP
####

locals {
  should_create_primary_eip                      = var.associate_public_ip_address == true && var.use_autoscaling_group == false
  should_create_eip_for_extra_network_interfaces = var.extra_network_interface_eips_count > 0 && var.use_autoscaling_group == false

  network_interface_with_eip_ids = local.should_create_eip_for_extra_network_interfaces ? [
    for i, network_interface in aws_network_interface.this_extra :
    network_interface.id
    if element(var.extra_network_interface_eips_enabled, i % var.extra_network_interface_count) == true
  ] : []
}

resource "aws_eip" "this_primary" {
  for_each = local.should_create_primary_eip ? { 0 = "enabled" } : {}

  domain = "vpc"
}

resource "aws_eip_association" "this_primary" {
  for_each = local.should_create_primary_eip ? { 0 = "enabled" } : {}

  network_interface_id = aws_network_interface.this_primary[0].id
  allocation_id        = aws_eip.this_primary["0"].id
}

resource "aws_eip" "this_extra" {
  count = local.should_create_eip_for_extra_network_interfaces ? var.extra_network_interface_eips_count : 0

  domain = "vpc"
}

resource "aws_eip_association" "this_extra" {
  count = local.should_create_eip_for_extra_network_interfaces ? var.extra_network_interface_eips_count : 0

  network_interface_id = element(local.network_interface_with_eip_ids, count.index)
  allocation_id        = element(aws_eip.this_extra[*].id, count.index)
}

####
# Key Pair
####

locals {
  key_pair_name = var.key_pair_create ? aws_key_pair.this["0"].key_name : var.key_pair_name
}

resource "aws_key_pair" "this" {
  for_each = var.key_pair_create ? { 0 = "enabled" } : {}

  key_name   = format("%s%s", var.prefix, var.key_pair_name)
  public_key = var.key_pair_public_key
  tags = merge(
    local.tags,
    var.key_pair_tags,
  )
}
