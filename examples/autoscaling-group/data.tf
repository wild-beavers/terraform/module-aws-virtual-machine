data "aws_vpc" "default" {
  default = true
}

data "aws_ec2_instance_type_offerings" "this" {
  filter {
    name   = "instance-type"
    values = ["t3.medium"]
  }

  location_type = "availability-zone-id"
}

data "aws_availability_zones" "available" {
  state = "available"

  filter {
    name   = "zone-id"
    values = distinct(data.aws_ec2_instance_type_offerings.this.locations)
  }

  filter {
    name   = "opt-in-status"
    values = ["opt-in-not-required"]
  }
}

data "aws_subnets" "all" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }

  filter {
    name   = "availability-zone"
    values = data.aws_availability_zones.available.names
  }

  filter {
    name   = "default-for-az"
    values = ["true"]
  }
}

data "aws_ssm_parameter" "default" {
  name = "/aws/service/ami-windows-latest/Windows_Server-2019-English-Full-Base"
}
