#####
# Context
#####

locals {
  prefix = "${random_string.start_letter.result}${random_string.this.result}"
}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

data "aws_caller_identity" "this" {}
data "aws_region" "this" {}

resource "aws_key_pair" "default" {
  key_name   = "${local.prefix}tftest"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD3F6tyPEFEzV0LX3X8BsXdMsQz1x2cEikKDEY0aIj41qgxMCP/iteneqXSIFZBp5vizPvaoIR3Um9xK7PGoW8giupGn+EPuxIA4cDM4vzOqOkiMPhz5XK0whEjkVzTo4+S0puvDZuwIsdiW9mxhJc7tgBNL0cYlWSYVkz4G/fslNfRPW5mYAM49f4fhtxPb5ok4Q2Lg9dPKVHO/Bgeu5woMc7RY0p1ej6D4CKFE6lymSDJpW0YHX/wqE9+cfEauh7xZcG0q9t2ta6F6fmX0agvpFyZo8aFbXeUBr7osSCJNgvavWbM/06niWrOvYX2xwWdhXmXSrbX8ZbabVohAK41 email@example.com"
}

resource "aws_kms_key" "default" {
  description             = "${local.prefix}tftest"
  deletion_window_in_days = 7
}

# Policy below Because AWS fails to create a default that simply works
resource "aws_kms_key_policy" "default" {
  key_id = aws_kms_key.default.id
  policy = jsonencode({
    Statement = [
      {
        Action = "kms:*"
        Effect = "Allow"
        Principal = {
          AWS = "*"
        }

        Resource = "*"
        Sid      = "Enable IAM User Permissions"
      },
    ]
    Version = "2012-10-17"
  })
}

resource "aws_security_group" "example" {
  name   = "${local.prefix}tftest1"
  vpc_id = data.aws_vpc.default.id
}

resource "aws_iam_instance_profile" "default" {}

resource "aws_lb" "example" {
  name               = "${local.prefix}tftestasg"
  internal           = true
  load_balancer_type = "network"
  subnets            = data.aws_subnets.all.ids
}

resource "aws_lb_target_group" "example" {
  name     = "${local.prefix}tftestasg"
  port     = 22
  protocol = "TCP"
  vpc_id   = data.aws_vpc.default.id
}

resource "aws_lb_listener" "example" {
  load_balancer_arn = aws_lb.example.arn
  port              = "22"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.example.arn
  }
}

#####
# ASG default
# Shows how to:
# - Create an ASG quickly without any option but defaults
# - Use default VPC/Subnets/Security Group
#####

module "default" {
  source = "../../"

  current_account_id = data.aws_caller_identity.this.account_id
  current_region     = data.aws_region.this.name

  use_autoscaling_group = true

  prefix = format("%s%s", local.prefix, "d")
}

#####
# ASG empty
# Shows how to:
# - create an ASG without any instance
#####

module "empty" {
  source = "../../"

  current_account_id = data.aws_caller_identity.this.account_id
  current_region     = data.aws_region.this.name

  use_autoscaling_group = true

  prefix = format("%s%s", local.prefix, "e")

  autoscaling_group_min_size = 0
}

#####
# ASG Options
# Shows how to:
# - count with ASG
# - Use Windows AMI
# - Use explicit subnets
# - Use non-default security group
# - Use various ASG options
# - Use specific instance type
# - Create key pair on first run and reuse it on subsequent runs
# - Add extra volumes
# - Create KMS key for the volumes
# - Add an IAM Instance Profile/Role
# - Register ASG instances to a Target Group
# - Add ASG schedules to change capacity
# - Add ASG instance refresh trigger
# - enable the instance metadata tags
# - set the metadata HTTP max hop to 2
# - set the metadata http token required
# - SNS Topic notifications
#####

resource "aws_sns_topic" "example" {
  name = format("%s%s", local.prefix, "o")
}

module "options" {
  source = "../../"

  count = 2

  current_account_id = data.aws_caller_identity.this.account_id
  current_region     = data.aws_region.this.name

  prefix = format("%s%s", local.prefix, "o")

  use_autoscaling_group = true
  name                  = "tftest-asg"
  launch_template_name  = format("%s-%02d", "tftest", count.index + 1)
  monitoring            = true

  ami                           = data.aws_ssm_parameter.default.value
  instance_type                 = "t3.medium"
  root_block_device_volume_size = 30

  tags = {
    Example = "TFTEST example"
  }

  instance_tags = {
    Name    = "tftest"
    Example = "TFTEST instance example"
  }

  vpc_security_group_ids = [aws_security_group.example.id]

  autoscaling_group_subnet_ids                = data.aws_subnets.all.ids
  autoscaling_group_name                      = format("%s-%02d", "tftestasg", count.index + 1)
  autoscaling_group_desired_capacity          = 1
  autoscaling_group_max_size                  = 3
  autoscaling_group_min_size                  = 1
  autoscaling_group_enabled_metrics           = ["GroupMinSize", "GroupMaxSize"]
  autoscaling_group_suspended_processes       = ["AlarmNotification"]
  autoscaling_group_health_check_type         = "ELB"
  autoscaling_group_target_group_arns         = [aws_lb_target_group.example.arn]
  autoscaling_group_wait_for_capacity_timeout = "15m"
  autoscaling_group_min_elb_capacity          = 1
  autoscaling_group_max_instance_lifetime     = 604800
  autoscaling_group_instance_refresh_triggers = ["tag", "capacity_rebalance", "max_instance_lifetime"]
  autoscaling_group_maintenance_policy = {
    min_healthy_percentage = 100
    max_healthy_percentage = 110
  }
  autoscaling_group_tags = {
    ASGName = "tftestasg"
  }

  key_pair_create = count.index == 0 ? true : false
  // This is because of prefix. Real world usage shouldn't be that complex: "tftest" would be sufficient.
  key_pair_name       = count.index == 0 ? "tftest" : format("%s%s%s", local.prefix, "o", "tftest")
  key_pair_public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD3F6tyPEFEzV0LX3X8BsXdMsQz1x2cEikKDEY0aIj41qgxMCP/iteneqXSIFZBp5vizPvaoIR3Um9xK7PGoW8giupGn+EPuxIA4cDM4vzOqOkiMPhz5XK0whEjkVzTo4+S0puvDZuwIsdiW9mxhJc7tgBNL0cYlWSYVkz4G/fslNfRPW5mYAM49f4fhtxPb5ok4Q2Lg9dPKVHO/Bgeu5woMc7RY0p1ej6D4CKFE6lymSDJpW0YHX/wqE9+cfEauh7xZcG0q9t2ta6F6fmX0agvpFyZo8aFbXeUBr7osSCJNgvavWbM/06niWrOvYX2xwWdhXmXSrbX8ZbabVohAK41 email@example.com"

  extra_volumes = {
    0 = {
      size        = 1
      device_name = "/dev/sdh"
    }
    1 = {
      size        = 1
      device_name = "/dev/sdi"
    }
  }

  volume_kms_key_name = format("%s%02d", "tftest", count.index)

  iam_instance_profile_iam_role_name        = "tftest"
  iam_instance_profile_iam_role_policy_arns = { 0 = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore" }

  autoscaling_schedules = {
    one = {
      name       = format("%s%s", "tftestone", count.index)
      recurrence = "0 1 * * *"
      start_time = timeadd(timestamp(), "30m")
    }
    two = {
      name             = format("%s%s", "tftesttwo", count.index)
      recurrence       = "0 8 * * *"
      desired_capacity = 1
      max_size         = 1
      start_time       = timeadd(timestamp(), "1h")
    }
  }
  autoscaling_notifications = {
    notif = {
      notifications = ["autoscaling:TEST_NOTIFICATION"]
      topic_arn     = aws_sns_topic.example.arn
    }
  }

  metadata_options_instance_metadata_tags_enabled = true
  metadata_options_http_put_response_hop_limit    = 2
  metadata_options_http_tokens_required           = true
}

#####
# ASG Externals
# Shows how to:
# - use default AMI with default subnet with default security group
# - use an external key pair
# - use an external IAM Instance Profile
# - use an external security group
# - use the module without an ELB
# - disabled instance refresh
#####

module "externals" {
  source = "../../"

  current_account_id = data.aws_caller_identity.this.account_id
  current_region     = data.aws_region.this.name

  prefix = format("%s%s", local.prefix, "x")

  use_autoscaling_group = true

  name = "tftest-asg-externals"

  tags = {
    Example = "TFTEST ASG externals"
  }

  launch_template_name = "tftest2"

  autoscaling_group_max_size                 = 1
  autoscaling_group_min_size                 = 1
  autoscaling_group_desired_capacity         = 1
  autoscaling_group_name                     = "tftest-asg-externals"
  autoscaling_group_health_check_type        = "EC2"
  autoscaling_group_instance_refresh_enabled = false

  vpc_security_group_ids    = [aws_security_group.example.id]
  key_pair_name             = aws_key_pair.default.key_name
  volume_kms_key_arn        = aws_kms_key.default.arn
  iam_instance_profile_name = aws_iam_instance_profile.default.name
}
