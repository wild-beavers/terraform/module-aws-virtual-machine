data "aws_vpc" "default" {
  default = true
}

data "aws_ec2_instance_type_offerings" "this" {
  filter {
    name   = "instance-type"
    values = ["t3.nano", "t3.small", "t3.micro"]
  }

  location_type = "availability-zone-id"
}

data "aws_availability_zones" "available" {
  state = "available"

  filter {
    name   = "zone-id"
    values = distinct(data.aws_ec2_instance_type_offerings.this.locations)
  }

  filter {
    name   = "opt-in-status"
    values = ["opt-in-not-required"]
  }
}

data "aws_subnets" "all" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }

  filter {
    name   = "availability-zone"
    values = data.aws_availability_zones.available.names
  }

  filter {
    name   = "default-for-az"
    values = ["true"]
  }
}

data "aws_subnet" "example" {
  id = tolist(data.aws_subnets.all.ids)[0]
}

data "aws_ssm_parameter" "linux" {
  name = "/aws/service/ami-amazon-linux-latest/amzn-ami-minimal-hvm-x86_64-ebs"
}

data "aws_iam_policy_document" "sts_instance" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "ec2.amazonaws.com"
      ]
    }
  }
}
