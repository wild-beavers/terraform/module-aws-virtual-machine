locals {
  prefix = "${random_string.start_letter.result}${random_string.this.result}"
}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

resource "aws_security_group" "example" {
  name   = "${local.prefix}1"
  vpc_id = data.aws_vpc.default.id
}

resource "aws_security_group" "example2" {
  name   = "${local.prefix}2"
  vpc_id = data.aws_vpc.default.id
}

resource "aws_network_interface" "example" {
  count = 3

  subnet_id = data.aws_subnet.example.id
}


resource "aws_iam_role" "external" {
  assume_role_policy = data.aws_iam_policy_document.sts_instance.json
  name_prefix        = "tftest${local.prefix}-external"
}

resource "aws_key_pair" "default" {
  key_name   = "tftest${local.prefix}"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD3F6tyPEFEzV0LX3X8BsXdMsQz1x2cEikKDEY0aIj41qgxMCP/iteneqXSIFZBp5vizPvaoIR3Um9xK7PGoW8giupGn+EPuxIA4cDM4vzOqOkiMPhz5XK0whEjkVzTo4+S0puvDZuwIsdiW9mxhJc7tgBNL0cYlWSYVkz4G/fslNfRPW5mYAM49f4fhtxPb5ok4Q2Lg9dPKVHO/Bgeu5woMc7RY0p1ej6D4CKFE6lymSDJpW0YHX/wqE9+cfEauh7xZcG0q9t2ta6F6fmX0agvpFyZo8aFbXeUBr7osSCJNgvavWbM/06niWrOvYX2xwWdhXmXSrbX8ZbabVohAK41 email@example.com"
}

resource "aws_kms_key" "default" {
  description             = local.prefix
  deletion_window_in_days = 7
}

resource "aws_iam_instance_profile" "default" {}

#####
# EC2 with lots of options
# Shows how to:
# - implicit use of external subnets
# - use external security groups
# - pass an AMI
# - assign 2 IPV4 addresses to primary network interface
# - choose instance type
# - change root block device options
# - disable root device encryption (not recommended)
# - use user data
# - create a key pair
# - create an instance profile with a role
# - enable monitoring
#####

module "options" {
  source = "../../"

  prefix = format("%s%s", local.prefix, "o")

  instance_type = "t3.micro"
  ami           = nonsensitive(data.aws_ssm_parameter.linux.value)
  name          = "tftest"

  ec2_subnet_id = data.aws_subnet.example.id

  vpc_security_group_ids = [aws_security_group.example.id, aws_security_group.example2.id]

  ec2_ipv4_addresses          = [cidrhost(data.aws_subnet.example.cidr_block, 11), cidrhost(data.aws_subnet.example.cidr_block, 12)]
  associate_public_ip_address = false
  ebs_optimized               = true
  monitoring                  = true
  user_data                   = "#!/bin/bash\n\necho test"
  instance_tags = {
    Env = "test"
  }

  volume_tags = {
    Name        = "tftest"
    Description = "Root volume of the EC2"
  }

  network_interface_tags = {
    Name = "tftest"
    Env  = "test"
  }

  key_pair_create     = true
  key_pair_name       = "tftest"
  key_pair_public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD3F6tyPEFEzV0LX3X8BsXdMsQz1x2cEikKDEY0aIj41qgxMCP/iteneqXSIFZBp5vizPvaoIR3Um9xK7PGoW8giupGn+EPuxIA4cDM4vzOqOkiMPhz5XK0whEjkVzTo4+S0puvDZuwIsdiW9mxhJc7tgBNL0cYlWSYVkz4G/fslNfRPW5mYAM49f4fhtxPb5ok4Q2Lg9dPKVHO/Bgeu5woMc7RY0p1ej6D4CKFE6lymSDJpW0YHX/wqE9+cfEauh7xZcG0q9t2ta6F6fmX0agvpFyZo8aFbXeUBr7osSCJNgvavWbM/06niWrOvYX2xwWdhXmXSrbX8ZbabVohAK41 email@example.com"

  iam_instance_profile_create               = true
  iam_instance_profile_name                 = "tftest"
  iam_instance_profile_path                 = "/test/"
  iam_instance_profile_iam_role_policy_arns = { 0 = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore" }
  iam_instance_profile_iam_role_description = "Nice test IAM Role for instance profile"
  iam_instance_profile_iam_role_name        = "tftest"
  iam_instance_profile_iam_role_tags = {
    Env  = "test"
    Type = "options"
  }

  root_block_device_volume_size = 10
  root_block_device_encrypted   = false
  root_block_device_volume_type = "gp3"

  extra_volumes = {
    1 = {
      size        = 1
      name        = "tftest"
      device_name = "/dev/sdh"
    }
  }

  tags = {
    tftest = "options"
  }
}

#####
# EC2 with extra volumes
# Shows how to:
# - create simple instance with defaults
# - create a KMS key with alias for extra volumes
# - attach a couple extra volumes to the instance
# - enable the instance metadata tags
# - set the metadata HTTP max hop to 2
# - set the metadata http token required
#####

module "with_volumes" {
  source = "../../"

  prefix = format("%s%s", local.prefix, "v")

  name = "tftest"

  volume_tags = {
    Name        = "tftest"
    Description = "Root volume"
  }

  volume_kms_key_create                   = true
  volume_kms_key_name                     = "tftest"
  volume_kms_key_alias                    = "tftest/test"
  volume_kms_key_customer_master_key_spec = "SYMMETRIC_DEFAULT"
  volume_kms_key_tags = {
    Description = "For extra volumes"
  }

  extra_volumes = {
    1 = {
      size                 = 4
      name                 = "tftest1"
      device_name          = "/dev/sdh"
      type                 = "io2"
      iops                 = 100
      multi_attach_enabled = true
    }

    2 = {
      size        = 1
      name        = "tftest2"
      device_name = "/dev/sdi"
      type        = "gp3"
      iops        = 3000
      throughput  = 125
    }
  }

  metadata_options_instance_metadata_tags_enabled = true
  metadata_options_http_put_response_hop_limit    = 2
  metadata_options_http_tokens_required           = true

  tags = {
    tftest = "volumes"
  }
}

#####
# EC2 with extra NICs & EIP
# Shows how to:
# - explicit use of default subnet, even with `var.ec2_subnet_id` provided.
# - create simple instance with defaults
# - associate public IP with an EIP
# - use non-default instance type to have up to 3 NICs
# - attach a couple extra network interfaces with options (extra private IPS, suffix offset, SG)
# - attach an extra EIP to the second extra network interface only
# - disabled the instance metadata endpoint
#####

module "with_nic_and_eips" {
  source = "../../"

  prefix = format("%s%s", local.prefix, "n")

  instance_type = "t3.small"

  name = "tftest"

  associate_public_ip_address = true

  use_default_subnet = true
  ec2_subnet_id      = data.aws_subnet.example.id

  extra_network_interface_count                = 2
  extra_network_interface_private_ips_counts   = [2, 1]
  extra_network_interface_security_group_count = 1
  extra_network_interface_security_group_ids   = [aws_security_group.example.id, aws_security_group.example2.id]
  extra_network_interface_source_dest_checks   = [true]
  extra_network_interface_tags = {
    NICName = "tftest"
  }

  extra_network_interface_eips_count   = 1
  extra_network_interface_eips_enabled = [false, true]

  metadata_options_http_endpoint_enabled = false

  tags = {
    tftest = "nic"
  }
}

#####
# EC2 with external resources
# Shows how to:
# - create three instances
# - use external subnets
# - use external security groups
# - pass an AMI
# - use external primary ENI
# - use external key pair
# - use external KMS key
# - create Instance Profile and reuse it on subsequent run
#####

module "externals" {
  source = "../../"

  count = 3

  prefix = format("%s%s", local.prefix, "e")

  name = format("tftest-%02d", count.index)

  ami = nonsensitive(data.aws_ssm_parameter.linux.value)

  ec2_subnet_id = data.aws_subnet.example.id

  vpc_security_group_ids = [aws_security_group.example.id, aws_security_group.example2.id]

  ec2_primary_network_interface_create      = false
  ec2_external_primary_network_interface_id = aws_network_interface.example[count.index].id
  key_pair_name                             = aws_key_pair.default.key_name
  volume_kms_key_arn                        = aws_kms_key.default.arn

  iam_instance_profile_create = count.index == 0 || count.index == 1 ? true : false
  # Specifically get the profile name of a previous iteration ("tftest1") for the iteration 2
  iam_instance_profile_name            = count.index == 2 ? format("%se%s", local.prefix, "tftest1") : format("%s%s", "tftest", tostring(count.index))
  iam_instance_profile_iam_role_create = count.index == 1 ? false : true
  iam_instance_profile_iam_role_id     = count.index == 1 ? aws_iam_role.external.id : null
}


#####
# EC2 with ignore change on userdata and ami
#####

module "ignore_change_ami_user_data" {
  source = "../../"

  prefix                                 = local.prefix
  use_autoscaling_group                  = false
  ec2_ignore_change_on_ami_and_user_data = true
}
