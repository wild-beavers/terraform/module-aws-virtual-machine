## 19.1.2

- fix: When using a default subnet check offering types to select appropriate az based on instance_type.

## 19.1.1

- fix: nothing new, just for pipeline re run.

## 19.1.0

- feat: adds `var.autoscaling_group_maintenance_policy` to control autoscaling group maintenance policy
- feat: adds `var.autoscaling_group_instance_refresh_standby_instances` to better control refresh strategy for ASG

## 19.0.0

- feat: (BREAKING) removes needless variables in recent version of terraform: `var.volume_kms_key_external_exist`
- feat: (BREAKING) removes needless variables in recent version of terraform: `var.iam_instance_profile_iam_role_policy_count`
- feat: (BREAKING) removes `var.volume_name`, `var.primary_network_interface_name`: manageable through `var.volume_tags`, `var.network_interface_tags`.
- feat: (BREAKING) `var.iam_instance_profile_iam_role_policy_arns` new requires a `map(string)` instead of `list(string)`
- feat: launch templates can now set tags for network interfaces.
- refactor: ASG tags are not propagated anymore, since all tags are set through launch template
- refactor: changes usage of `cpu_core_count` and `cpu_threads_per_core` to fix deprecated code
- fix: `var.iam_instance_profile_path` was not correctly computed for `precomputed` outputs
- fix: tags for ASG and launch template improperly sanitized: null value were crashing the module

## 18.0.0

- feat: makes sure that non-null variables cannot be nullable
- feat: adds `var.ec2_user_data_replace_on_change` variable.
- feat: adds `var.autoscaling_notifications` to manage asg notifications.
- feat: adds `var.current_region` and `var.current_account_id` optional variables.
- feat: (BREAKING) removes useless `var.autoscaling_group_subnet_ids_count`
- tech: (BREAKING) bumps minimal terraform version to 1.8
- refactor: (BREAKING) renames `var.ec2_metadata_options_http_endpoint_enabled` to `var.metadata_options_http_endpoint_enabled`
- refactor: (BREAKING) changes outputs to new standards:
   - `launch_template_id` => `aws_launch_template.id`
   - `launch_template_arn` => `aws_launch_template.arn`
   - `launch_template_default_version` => `aws_launch_template.default_version`
   - `launch_template_latest_version` => `aws_launch_template.latest_version`
   - `autoscaling_group_id` => `aws_autoscaling_group.id`
   - `autoscaling_group_arn` => `aws_autoscaling_group.arn`
   - `ec2_arn` => `aws_instance.arn`
   - `ec2_id` => `aws_instance.id`
   - `ec2_private_ip` => `aws_instance.private_ip`
   - `ec2_primary_network_interface_id` => `aws_instance.primary_network_interface_id`
   - `ec2_private_dns` => `aws_instance.private_dns`
   - `ec2_public_dns` => `aws_instance.public_dns`
   - `ec2_public_ip` => `aws_instance.public_ip`
   - `iam_instance_profile_id` => `aws_iam_instance_profile.id`
   - `iam_instance_profile_arn` => `aws_iam_instance_profile.arn`
   - `iam_instance_profile_unique_id` => `aws_iam_instance_profile.unique_id`
   - `iam_instance_profile_iam_role_arn` => `aws_iam_role.arn`
   - `iam_instance_profile_iam_role_id` => `aws_iam_role.id`
   - `iam_instance_profile_iam_role_unique_id` => `aws_iam_role.unique_id`
   - `key_pair_name` => `aws_key_pair.key_name`
   - `key_pair_id` => `aws_key_pair.id`
   - `key_pair_fingerprint` => `aws_key_pair.fingerprint`
   - `kms_key_id` => `aws_kms_key.id`
   - `eip_public_ips.0.primary` => `aws_eips.primary.public_ip`
   - `eip_public_ips.xxx.extra` => `aws_eips[xxx].public_ip`
   - `eip_public_dns.0.primary` => `aws_eips.primary.public_dns`
   - `eip_public_dns.xxx.extra` => `aws_eips[xxx].public_dns`
   - `eip_network_interface_ids.0.primary` => `aws_eips.primary.network_interface_id`
   - `eip_network_interface_ids.xxx.extra` => `aws_eips[xxx].network_interface_id`
   - `extra_volume_ids[xxx]` => `aws_ebs_volumes[xxx].id`
   - `extra_volume_arns[xxx]` => `aws_ebs_volumes[xxx].arn`
   - `network_interface_ids.primary.0` => `aws_network_interfaces.primary.id`
   - `network_interface_mac_addresses.primary.0` => `aws_network_interfaces.primary.mac_address`
   - `network_interface_private_dns_names.primary.0` => `aws_network_interfaces.primary.private_dns_name`
   - `network_interface_private_ips.primary.0` => `aws_network_interfaces.primary.private_ips`
   - `network_interface_ids.extra.xxx` => `aws_network_interfaces[xxx].id`
   - `network_interface_mac_addresses.extra.xxx` => `aws_network_interfaces[xxx].mac_address`
   - `network_interface_private_dns_names.extra.xxx` => `aws_network_interfaces[xxx].private_dns_name`
   - `network_interface_private_ips.extra.xxx` => `aws_network_interfaces[xxx].private_ips`
   - `network_interface_eips` => `*REMOVED*`
- refactor: (BREAKING) tags handling modernization. Adds `origin`, modify precedences.
- refactor: (BREAKING) remove the handling of suffixes by the module. Removes:
   - `var.use_num_suffix`
   - `var.num_suffix_digits`
   - `var.num_suffix_offset`
   - `var.extra_network_interface_num_suffix_offset`
- refactor: (BREAKING) removes outputs:
   - `availability_zones`
   - `subnet_ids`
- refactor: (BREAKING) moves `var.autoscaling_schedule_`* variables into `var.autoscaling_schedules` map of objects
- refactor: changes the 10s waiting for the creation of the ASG
- fix: AMI was marked as sensitive
- fix: `aws_iam_service_linked_role` name length was not respected in all situations

## 17.4.1

- fix: instance tags was in fact the volume tags

## 17.4.0

- feat: (ec2) add possibility to ignore change on AMI and userdata. This is controlled by `var.ec2_ignore_change_on_ami_and_user_data`
- chore: bump pre-commit hooks

## 17.3.2

- fix: passes tags to `aws_iam_service_linked_role`
- chore: bump pre-commit hooks

## 17.3.1

- fix: allow default module tags to be overridden

## 17.3.0

- feat: force IMDS V2 only
- doc: update changelog to comply with Wildbeaver standards
- test: rename test `main.tf` filename to `deploy.tf` to comply with CI
- test: rename test `disabled` test case to `disable` to comply with CI

## 17.2.0

- feat: adds tags to IAM Instance Profile by introducing a new variable `iam_instance_profile_iam_instance_profile_tags`
- feat: pin AWS provider to `5+`
- fix: replace deprecated `vpc` argument by `domain` on AWS EIP resource
- chore: use the correct syntax for required provider

## 17.1.0

- feat: change `var.autoscaling_group_instance_refresh_triggers` default value from `tags` to `tag` to make it compatible with aws provider 5+
- tests: change `aws_subnet_ids` to `aws_subnets` to make it compatible with aws provider 5+

## 17.0.0

- feat: (BREAKING) bump minimal Terraform version to `1.3`
- feat: (BREAKING) bump minimal AWS provider version to `4.18.0`
- refactor: (BREAKING) change `extra_volumes_*` to a single map called `extra_volumes`. The replacement matrix is:
   - `extra_volume_device_names` to `extra_volumes.device_name`
   - `extra_volume_name` to `extra_volumes.name`
   - `extra_volume_sizes` to `extra_volumes.size`
   - `extra_volume_types` to `extra_volumes.type`
- refactor: (BREAKING) change `extra_volume_ids` and `extra_volume_arns` outputs to a map
- feat: adds `throughput` for root device controlled by `var.root_block_device_throughput`
- feat: adds new feature to extra volumes: throughput, iops, final snapshot, multi attach and snapshot id
- feat: fully support `gp3` and `io2` device type
- fix: replace deprecated `aws_subnet_ids` data source by `aws_subnets`
- fix: remove deprecated attribute for ASG attachment
- doc: remove README on example to reflect new WildBeaver standards
- test: remove Jenkinsfile
- chore: bump pre-commit hooks
- chore: update LICENSE file to include the current date

## 16.1.2

- tech: Add gitlab-ci yml
- fix: Fetch az that have `t3.medium Windows` available in examples/autoscaling-group

## 16.1.1

- maintenance: `null` provider version set to `>= 2.1`
- maintenance: tflint issues

## 16.1.0

- feat: add metadata option support by introducing the `ec2_metadata_options_http_endpoint_enabled`, `metadata_options_http_tokens_required`, `metadata_options_http_put_response_hop_limit` and `metadata_options_instance_metadata_tags_enabled` variables
- feat: allow to use an external role for IAM instance profile by introducing the `iam_instance_profile_iam_role_create` and `iam_instance_profile_iam_role_id` variables.
- doc: add the EC2 metadata limitation in README
- chore: bump pre-commit hooks

## 16.0.2

- fix: allows GP3 for volumes

## 16.0.1

- fix: wrong output condition on `iam_instance_profile_iam_role_id` and `iam_instance_profile_iam_role_unique_id`
- chore: bump pre-commit version

## 16.0.0

- feat: (BREAKING) respect `use_num_suffix` for ASG and launch template too

## 15.0.0

- feat: (BREAKING) adds ability to refresh instances in autoscaling group (enable by default)

## 14.0.1

- fix: restores `var.ec2_use_default_subnet` but make it optional

## 14.0.0

- feat: (BREAKING) simplifies the handling of tags for volume, with a single variable `volume_tags` instead of 2.
- feat: (BREAKING) simplifies the handling of name for volume, with a single variable `volume_name` instead of 2.
- fix: makes EC2 name use "incremental" value when it is set
- fix: makes EC2 key pair name use "incremental" value when it is set

## 13.0.0

- feat: removes `var.ec2_use_default_subnet` as it is not needed anymore in recent Terraform

## 12.0.0

- feat: adds `var.autoscaling_group_protect_from_scale_in` for ASGs
- feat: adds `var.autoscaling_group_capacity_rebalance` for ASGs
- maintenance (BREAKING): bumps Terraform required version to 0.15.0
- chore: bump pre-commit hooks to fix jenkins test
- doc: updates LICENSE
- test: cleanup examples
- test: be more resilient to parallel builds if multiple subnets exists in the default VPC

## 11.0.0

- feat (BREAKING): upgrades to support Terraform 0.13 properly
- feat (BREAKING): adds validation to all the variables
- feat: adds `aws_autoscaling_schedule` to add ASG schedules
- feat: adds `var.volume_kms_key_external_exist`
- refactor (BREAKING): removes convoluted loops to handle module count
- refactor (BREAKING): renames `ec2_external_primary_network_insterface_id` to `var.ec2_external_primary_network_interface_id`
- refactor (BREAKING): removes `var.use_external_primary_network_interface`
- refactor (BREAKING): changes multiple variables types from lists to simple types
- refactor (BREAKING): rename `external*volumes` to `extra*volumes`
- refactor (BREAKING): rename `this` extra NICs resources to `this_extra`
- refactor (BREAKING): remove `iam_instance_profile_external_name` to use `iam_instance_profile_name` directly
- refactor (BREAKING): all EC2 outputs are now singular instead of plural
- refactor (BREAKING): `var.use_num_suffix` is now `true` by default
- refactor (BREAKING): transform EIP outputs in objects with key primary and extra
- refactor (BREAKING): rename and change in objects `extra_network_interface_XXX` to `network_interface_XXX` containing both primary and extra
- refactor (BREAKING): removes `extra_network_interface_public_ips` output
- refactor (BREAKING): most names are now not incremental anymore, except extra volumes and NICs
- refactor: Split resources into more digestable, smaller files
- doc: changes most variables descriptions to be more accurate and give more insight
- doc: updates README: update what the module does and improves `limitations` section
- maintenance: pins pre-commit dependencies to latest versions
- fix: fix the ability to inject external primary network interface for EC2
- fix: also use `var.prefix` for IAM Role and Instance Profile
- fix: creates a KMS grant when KMS and ASG is used, to allow ASG to use the key for decrypting volumes

## 10.0.0

- feat (BREAKING): do not create instance profile by default
- feat (BREAKING): default instance type is changes to t3.small to cheaper t3.nano
- feat: if no AMI is specified, now uses the latest amazon linux AMI
- test: adds a default baby example
- doc: improves some variables description

## 9.0.0

- feat (BREAKING): add prefix (`volume_kms_key_alias` is now automaticaly prefixed by `alias/`)
- feat: Add external primary network inteface to use an external ENI for EC2 instances
- chore: fix provider assume role

## 8.0.0

- feat(BREAKING): uses external resource to create primary network interface for EC2
- feat(BREAKING): uses external resource to create primary EIP when needed
- feat(BREAKING): removes eip_create as it now equals associate_public_ip_address
- feat: adds ipv4_address_count to set primary netint IPv4 addresses
- feat: adds description to primary network interface
- feat: adds generic description to extra network interfaces
- feat: adds names to the network interface and extra network interfaces
- refactor: removes unused launch_template_ipv4_address_count

## 7.1.2

- fix: adds suffixes for external volumes and KMS keys when us_num_suffix=true

## 7.1.1

- fix: typo in versions.tf to be usable with terraform 0.13

## 7.1.0

- feat: removes managed-by=Terraform tags for ASG instances
- fix: makes sure zipmap don’t makes error when a resources is destroyed with target

## 7.0.0

- feat: Add a default name for the root block device of EC2 instances
- feat (BREAKING): start external volume index at 2

## 6.2.0

- feat: Allow for delete on termination on root block device as a variable

## 6.1.1

- fix: fix AutoScaling group creation with latest AWS provider
- fix: fix `cpu_options` for ASG
- fix: fix not working dynamic blocks throughout the module
- tech: update possible version for AWS provider to 2.60 and up
- test: rename `no-instances-no-volumes` example to `disabled`
- test: add an example with Windows machine

## 6.1.0

- feat: allows to specify a numeric suffix offset

## 6.0.4

- tech: Add example to test asg without ALB/NLB
- fix: Changed type of some variables

## 6.0.3

- fix: output KMS key ID when it is created by the module
- fix: makes `ephemeral_block_devices` a list type
- test: checks idempotency when using `ec2_volume_tags` and `external_volume_tags`

## 6.0.2

- fix: uses /dev/sda1 as root block device

## 6.0.1

- fix: uses var.name for instance name even with launch template
- refactor: removes deprecated variable `iam_instance_profile`
- doc: better describes variables for both instance and launch template

## 6.0.0

- refactor (BREAKING): replace deprecated `launch_configuration` by `launch_template`
- refactor (BREAKING): renames some EC2-specific variable `ec2_` prefix for what is shared with launch template
- fix: do not create extra network interfaces when ASG is selected
- fix: do not create EIP when ASG is selected

## 5.0.0

- feat (BREAKING): handle IAM instance profile

## 4.1.0

- feat: handle elastic IPs for EC2 instances
- feat: handle elastic IPs for network interfaces

## 4.0.1

- fix: do not fetch some default resources on AWS account when not needed

## 4.0.0

- feat: handle multiple `aws_network_interface` resources
- refactor (BREAKING deployed resources): makes outputs map of lists for volumes, will change the order of creation of volumes
- refactor: prefix EC2-specific outputs with `ec2_`
- tech: adds validation with terraform tflint

## 3.0.0

- fix: BREAKING Use default KMS key for volume encryption by default

## 2.1.1

- fix: required version in greater or equal to 2.54, not 2.54.0
- fix: suffix name contain spaces instad of number

## 2.1.0

- feat: handle `aws_key_pair` resource locally or externally
- feat: adds a tag for all resources: Provider=Terraform

## 2.0.0

- refactor (BREAKING): merge the two `aws_instance` resources (t instance and the other) to one single resource.
- refactor: removes `credit_specifications` from outputs because it’s also variable.
- fix: do not create a KMS key if KMS ARN is given.
- fix: lowers the risk of conflicting tags in AutoScaling Group that would break idempotency

## 1.1.0

- feat: handle AutoScaling group and Launch Configuration resources
- feat: handle volume types for external volumes
- refactor: better handling of numeric suffix, toggleable by setting it to 0 and using numeric value instead of string
- refactor: reorganizes variables to separate EC2-specific, ASG-specific and common

## 1.0.0

- feat: adds more accurate type validation for variables
- feat: adds `host_id`, `cpu_core_count`, `cpu_threads_per_core` arguments for EC2
- feat: adds `customer_master_key_spec` and `policy` arguments for KMS key for all volumes
- feat: now possible to encrypt root volume with the same KMS key as external volumes
- feat: If no security group is given, VPC default security group is used
- refactor: adapts code to work with terraform0.12
- refactor: `external_volume_kms_***` variables are now `volume_kms_***` because the key can be used for root device
- refactor: Remove ebs_block_device as we should always use external block device in this module
- test: adds test for modifying root block device
- tech: bumps pre-commit config versions
- doc: adds CHANGELOG

## 0.0.0

- Fork from terraform-module-aws-ec2 module
