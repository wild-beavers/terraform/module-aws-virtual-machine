####
# KMS
####

locals {
  should_create_kms_key          = var.volume_kms_key_create && (var.root_block_device_encrypted || local.extra_volume_count > 0)
  should_grant_asg_to_access_key = var.root_block_device_encrypted && var.use_autoscaling_group && local.should_create_kms_key

  volume_kms_key_arn = local.should_create_kms_key ? aws_kms_key.this_volume["0"].arn : var.volume_kms_key_arn
}

resource "aws_kms_key" "this_volume" {
  for_each = local.should_create_kms_key ? { 0 = "enabled" } : {}

  description              = "KMS key for ${format("%s%s", var.prefix, var.name)} instance(s) volume(s)."
  customer_master_key_spec = var.volume_kms_key_customer_master_key_spec
  policy                   = var.volume_kms_key_policy

  tags = merge(
    local.tags,
    {
      Name = format("%s%s", var.prefix, var.volume_kms_key_name)
    },
    var.volume_kms_key_tags,
  )
}

resource "aws_kms_alias" "this_volume" {
  for_each = local.should_create_kms_key ? { 0 = "enabled" } : {}

  name          = format("alias/%s%s", var.prefix, var.volume_kms_key_alias)
  target_key_id = aws_kms_key.this_volume["0"].key_id
}

resource "aws_kms_grant" "this_volume" {
  for_each = local.should_grant_asg_to_access_key ? { 0 = "enabled" } : {}

  name              = "AllowASGToAccessKMS"
  key_id            = local.volume_kms_key_arn
  grantee_principal = aws_iam_service_linked_role.asg[0].arn
  operations        = ["Encrypt", "Decrypt", "GenerateDataKey", "DescribeKey", "CreateGrant", "GenerateDataKeyWithoutPlaintext", "ReEncryptFrom", "ReEncryptTo", "RetireGrant"]
}
