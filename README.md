# Terraform module: Virtual Machine (EC2, AutoScaling Group)

This module have the following features, they are all optional:

- EC2 instance or one AutoScaling Group with X capacity.
- X extra volumes, encrypted by default, with optional KMS key.
- X extra network interfaces attached to the EC2 instance.
- A Key Pair.
- An Instance Profile.
- Elastic IPS for the instance and/or for specific extra network interfaces.

## Limitations

- AWS does not handle external volumes with AutoScaling Groups.
  Because of this, if an AutoScaling Group with one or more EBS volume is destroy, the EBS volumes would be preserved, resulting in phantom volumes (unseen by Terraform).
  That’s why every extra volumes within an AutoScaling group will always be destroyed by using this module (delete_on_termination = true).
- Same kind of resources will share the same tags. It’s not possible to assign tag to a specific EIP, as specific volume or a specific network interface.

## Notes

To install pre-commit hooks: `pre-commit install`.
It will automatically `validate`, `fmt` and update *README.md* for you.

The variable `root_block_device_delete_on_termination` set to false is not tested because it will create resources that will persist a terraform build.
Therefore until we find a more permanent solution for this we do NOT test this feature.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 1.8 |
| aws | >= 5 |
| null | >= 2.1 |
| time | >= 0 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 5 |
| time | >= 0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_autoscaling_attachment.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_attachment) | resource |
| [aws_autoscaling_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_group) | resource |
| [aws_autoscaling_notification.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_notification) | resource |
| [aws_autoscaling_schedule.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_schedule) | resource |
| [aws_ebs_volume.this_extra](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ebs_volume) | resource |
| [aws_eip.this_extra](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip) | resource |
| [aws_eip.this_primary](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip) | resource |
| [aws_eip_association.this_extra](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip_association) | resource |
| [aws_eip_association.this_primary](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip_association) | resource |
| [aws_iam_instance_profile.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | resource |
| [aws_iam_role.this_instance_profile](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.this_instance_profile](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_service_linked_role.asg](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_service_linked_role) | resource |
| [aws_instance.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | resource |
| [aws_instance.this_ignore_ami_user_data_changes](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | resource |
| [aws_key_pair.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/key_pair) | resource |
| [aws_kms_alias.this_volume](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_grant.this_volume](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_grant) | resource |
| [aws_kms_key.this_volume](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_launch_template.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/launch_template) | resource |
| [aws_network_interface.this_extra](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_interface) | resource |
| [aws_network_interface.this_primary](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_interface) | resource |
| [aws_network_interface_attachment.this_extra](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_interface_attachment) | resource |
| [aws_network_interface_sg_attachment.this_extra](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_interface_sg_attachment) | resource |
| [aws_volume_attachment.this_extra](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/volume_attachment) | resource |
| [time_sleep.wait_service_linked_role_asg](https://registry.terraform.io/providers/hashicorp/time/latest/docs/resources/sleep) | resource |
| [aws_availability_zones.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones) | data source |
| [aws_caller_identity.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_ec2_instance_type_offerings.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ec2_instance_type_offerings) | data source |
| [aws_iam_policy_document.sts_instance](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_region.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |
| [aws_security_group.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/security_group) | data source |
| [aws_ssm_parameter.default_ami](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ssm_parameter) | data source |
| [aws_subnet.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet) | data source |
| [aws_subnets.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnets) | data source |
| [aws_vpc.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| ami | AMI to use for the EC2 instance (or the launch template). Default: latest AWS linux AMI - CAREFUL: when using the default, the AMI ID could get updated, thus triggering a destroy/recreate of your instances. Besides testing, it's recommended to set a value. | `string` | `""` | no |
| associate\_public\_ip\_address | Whether to associate a public ip address for the EC2 instance (or launch template) main network interface. | `bool` | `false` | no |
| autoscaling\_group\_capacity\_rebalance | Indicates whether capacity rebalance is enabled. Otherwise, capacity rebalance is disabled. See [the official documentation on capacity rebalance](https://docs.aws.amazon.com/autoscaling/ec2/userguide/capacity-rebalance.html). | `bool` | `false` | no |
| autoscaling\_group\_default\_cooldown | The amount of time, in seconds, after a scaling activity completes before another scaling activity can start. | `number` | `-1` | no |
| autoscaling\_group\_desired\_capacity | Number of instances to immediately launch in the AutoScaling Group. If not specified, defaults to `var.autoscaling_group_min_size`. | `number` | `null` | no |
| autoscaling\_group\_enabled\_metrics | A list of metrics to collect. The allowed values are `GroupDesiredCapacity`, `GroupInServiceCapacity`, `GroupPendingCapacity`, `GroupMinSize`, `GroupMaxSize`, `GroupInServiceInstances`, `GroupPendingInstances`, `GroupStandbyInstances`, `GroupStandbyCapacity`, `GroupTerminatingCapacity`, `GroupTerminatingInstances`, `GroupTotalCapacity` and `GroupTotalInstances`. | `set(string)` | `[]` | no |
| autoscaling\_group\_health\_check\_grace\_period | Time (in seconds) after instance comes into service before checking health. | `number` | `-1` | no |
| autoscaling\_group\_health\_check\_type | Controls how health checking is done on `EC2` level or on `ELB` level. When using a load balancer `ELB` is recommended. | `string` | `null` | no |
| autoscaling\_group\_instance\_refresh\_enabled | Whether instances should be refreshed when a change occurs in the launch template. | `bool` | `true` | no |
| autoscaling\_group\_instance\_refresh\_min\_healthy\_percentage | The amount of capacity in the Auto Scaling group that must remain healthy during an instance refresh to allow the operation to continue, as a percentage of the desired capacity of the Auto Scaling group. Defaults to `90`. | `number` | `90` | no |
| autoscaling\_group\_instance\_refresh\_standby\_instances | Behavior when encountering instances in the `Standby` state in are found. Available behaviors are `Terminate`, `Ignore`, and `Wait`. Default is `Ignore`. | `string` | `"Ignore"` | no |
| autoscaling\_group\_instance\_refresh\_strategy | The strategy to use for instance refresh. The only allowed value for now is `Rolling`. | `string` | `"Rolling"` | no |
| autoscaling\_group\_instance\_refresh\_triggers | Set of additional property names that will trigger an Instance Refresh. A refresh will always be triggered by a change in any of `launch_configuration`, `launch_template`, or `mixed_instances_policy`. | `list(string)` | <pre>[<br/>  "tag"<br/>]</pre> | no |
| autoscaling\_group\_maintenance\_policy | An instance maintenance policy for the AutoScaling Group. Only used when `var.use_autoscaling_group` is `true`.<br/>  - min\_healthy\_percentage (required, number): Lower limit on the number of instances that must be in the InService state with a healthy status, only during an instance replacement activity.<br/>  - max\_healthy\_percentage (required, number): Upper limit on the number of instances that are in the InService or Pending state with a healthy status, only during an instance replacement activity. | <pre>object({<br/>    min_healthy_percentage = number<br/>    max_healthy_percentage = number<br/>  })</pre> | `null` | no |
| autoscaling\_group\_max\_instance\_lifetime | The maximum amount of time, in seconds, that an instance can be in service, values must be either equal to `0` or between `604800` and `31536000` seconds. | `number` | `0` | no |
| autoscaling\_group\_max\_size | The maximum size of the AutoScaling Group. | `number` | `1` | no |
| autoscaling\_group\_metrics\_granularity | The granularity to associate with the metrics to collect. The only valid value is `1Minute`. Default is `1Minute`. | `string` | `null` | no |
| autoscaling\_group\_min\_elb\_capacity | Setting this causes Terraform to wait for this number of instances from this autoscaling group to show up healthy in the ELB only on creation. Updates will not wait on ELB instance number changes. [See documentation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_group#waiting-for-capacity). | `number` | `null` | no |
| autoscaling\_group\_min\_size | The minimum size of the AutoScaling Group. | `number` | `1` | no |
| autoscaling\_group\_name | The name of the AutoScaling Group. By default generated by Terraform. | `string` | `""` | no |
| autoscaling\_group\_protect\_from\_scale\_in | Allows setting instance protection. The Auto Scaling Group will not select instances with this setting for termination during scale in events. | `bool` | `false` | no |
| autoscaling\_group\_subnet\_ids | IDs of the subnets to be used by the AutoScaling Group. If empty, all the default subnets of the current region will be used. | `list(string)` | `[]` | no |
| autoscaling\_group\_suspended\_processes | A list of processes to suspend for the AutoScaling Group. The allowed values are `Launch`, `Terminate`, `HealthCheck`, `ReplaceUnhealthy`, `AZRebalance`, `AlarmNotification`, `ScheduledActions`, `AddToLoadBalancer`. Note that if you suspend either the Launch or Terminate process types, it can prevent your autoscaling group from functioning properly. | `set(string)` | `[]` | no |
| autoscaling\_group\_tags | Tags specific to the AutoScaling Group. Will be merged with var.tags. | `map(string)` | `{}` | no |
| autoscaling\_group\_target\_group\_arns | A list of aws\_alb\_target\_group ARNs, for use with Application or Network Load Balancing. | `list(string)` | `[]` | no |
| autoscaling\_group\_termination\_policies | A list of policies to decide how the instances in the auto scale group should be terminated. The allowed values are `OldestInstance`, `NewestInstance`, `OldestLaunchConfiguration`, `ClosestToNextInstanceHour`, `OldestLaunchTemplate`, `AllocationStrategy`, `Default`. | `list(string)` | `[]` | no |
| autoscaling\_group\_wait\_for\_capacity\_timeout | A maximum duration that Terraform should wait for ASG instances to be healthy before timing out. Setting this to '0' causes Terraform to skip all Capacity Waiting behavior. | `string` | `null` | no |
| autoscaling\_group\_wait\_for\_elb\_capacity | Setting this will cause Terraform to wait for exactly this number of healthy instances from this autoscaling group in all attached load balancers on both create and update operations. (Takes precedence over `var.min_elb_capacity` behavior.). | `number` | `null` | no |
| autoscaling\_notifications | Auto scaling notifications. Only used when `var.use_autoscaling_group` is `true`.<br/>Keys are free values.<br/><br/>  * notifications (required, list(string)): List of Notification Types that trigger notifications. Acceptable values are documented in the [AWS documentation here](https://docs.aws.amazon.com/AutoScaling/latest/APIReference/API_NotificationConfiguration.html).<br/>  * topic\_arn     (required, string): Topic ARN for notifications to be sent through | <pre>map(object({<br/>    notifications = list(string)<br/>    topic_arn     = string<br/>  }))</pre> | `{}` | no |
| autoscaling\_schedules | Auto scaling schedules. Only used when `var.use_autoscaling_group` is `true`.<br/>Keys are free values.<br/><br/>  * name             (required, string): The name of this scaling action.<br/>  * min\_size         (optional, number): The minimum size of the Auto Scaling group. Set to `-1` if you don't want to change the minimum size at the scheduled time. Defaults to `0`.<br/>  * max\_size         (optional, number): The maximum size of the Auto Scaling group. Set to `-1` if you don't want to change the maximum size at the scheduled time. Defaults to `0`.<br/>  * desired\_capacity (optional, number): The initial capacity of the Auto Scaling group after the scheduled action runs and the capacity it attempts to maintain. Set to `-1` if you don't want to change the desired capacity at the scheduled time. Defaults to `0`.<br/>  * recurrence       (optional, string): The recurring schedule for this action specified using the Unix cron syntax format.<br/>  * start\_time       (optional, string): The date and time for the recurring schedule to start, in UTC with the format "`YYYY-MM-DDThh:mm:ssZ`" (e.g. "`2021-06-01T00:00:00Z`").<br/>  * end\_time         (optional, string): The date and time for the recurring schedule to end, in UTC with the format "`YYYY-MM-DDThh:mm:ssZ`" (e.g. "`2021-06-01T00:00:00Z`").<br/>  * time\_zone        (optional, string): Specifies the time zone for a cron expression. Valid values are the canonical names of the IANA time zones (such as Etc/GMT+9 or Pacific/Tahiti). | <pre>map(object({<br/>    name             = string<br/>    min_size         = optional(number)<br/>    max_size         = optional(number)<br/>    desired_capacity = optional(number)<br/>    recurrence       = optional(string)<br/>    start_time       = optional(string)<br/>    end_time         = optional(string)<br/>    time_zone        = optional(string)<br/>  }))</pre> | `{}` | no |
| cpu\_core\_count | Sets the number of CPU cores for an instance (or launch template). This option is only supported on creation of instance type that support CPU Options CPU Cores and Threads Per CPU Core Per Instance Type - specifying this option for unsupported instance types will return an error from the EC2 API. | `number` | `null` | no |
| cpu\_credits | The credit option for CPU usage. Can be `standard` or `unlimited`. For T type instances. T3 instances are launched as unlimited by default. T2 instances are launched as standard by default. | `string` | `""` | no |
| cpu\_threads\_per\_core | If set to to 1, hyperthreading is disabled on the launched instance (or launch template). Defaults to 2 if not set. See Optimizing CPU Options for more information (has no effect unless `var.cpu_core_count` is also set). | `number` | `null` | no |
| current\_account\_id | The account where this module is run. | `string` | `""` | no |
| current\_region | The region where this module is run. | `string` | `""` | no |
| disable\_api\_termination | If true, enables EC2 Instance (or launch template) termination protection. **This is NOT recommended** as it will prevent Terraform to destroy and block your pipeline. | `bool` | `false` | no |
| ebs\_optimized | If true, the launched EC2 instance (or launch template) will be EBS-optimized. Note that if this is not set on an instance type that is optimized by default then this will show as disabled but if the instance type is optimized by default then there is no need to set this and there is no effect to disabling it. | `bool` | `null` | no |
| ec2\_external\_primary\_network\_interface\_id | ID of the primary Network Interface to be attached to EC2 instance. This value must be given if `var.ec2_primary_network_interface_create` is `false`. | `string` | `null` | no |
| ec2\_ignore\_change\_on\_ami\_and\_user\_data | Whether to ignore change on AMI and userdata. Enabling this won't update or re-create the instance if AMI or userdata are updated. | `bool` | `false` | no |
| ec2\_ipv4\_addresses | Specify one or more IPv4 addresses from the range of the subnet to associate with the primary network interface. | `list(string)` | `[]` | no |
| ec2\_ipv6\_addresses | Specify one or more IPv6 addresses from the range of the subnet to associate with the primary network interface. | `list(string)` | `[]` | no |
| ec2\_primary\_network\_interface\_create | Whether to create a primary Network Interface to be attached to EC2 instance. Ignored if `var.use_autoscaling_group` is `true`. If `false`, a value for `var.ec2_external_primary_network_interface_id` will be expected. | `bool` | `true` | no |
| ec2\_source\_dest\_check | Controls if traffic is routed to the instance when the destination address does not match the instance. Used for NAT or VPNs. | `bool` | `true` | no |
| ec2\_subnet\_id | Subnet ID where to provision all the instances. Can be used instead or along with `var.subnet_ids`. | `string` | `null` | no |
| ec2\_user\_data\_replace\_on\_change | When set to `true` in combination with `var.user_data`, will trigger a destroy and recreate of the instances. | `bool` | `false` | no |
| ephemeral\_block\_devices | Customize Ephemeral (also known as Instance Store) volumes on the EC2 instance (or launch template):<br/>  * device\_name (required, string): The name of the block device to mount on the instance.<br/>  * virtual\_name (optional, string): The Instance Store Device Name (e.g. "ephemeral0").<br/>  * no\_device (optional, string): Suppresses the specified device included in the AMI's block device mapping. | `list(any)` | `[]` | no |
| extra\_network\_interface\_count | How many extra network interface to create for the EC2 instance. This has no influence on the primary Network Interface. Ignored if `var.use_autoscaling_group` is `true`. | `number` | `0` | no |
| extra\_network\_interface\_eips\_count | How many extra Network Interfaces will have a public Elastic IP. Should be the exact number of `true`s in the `var.extra_network_interface_eips_enabled` list. Ignored if `var.use_autoscaling_group` is `true`. | `number` | `0` | no |
| extra\_network\_interface\_eips\_enabled | List of boolean that indicates Whether the extra Network Interface should have an Elastic IP or not. To disable/enable the EIP for specific NICs, use `false`/`true` respectively of the order of extra Network Interfaces. Should have as many `true`s as the number define in `var.extra_network_interface_eips_count`. Ignored if `var.use_autoscaling_group` is `true`. | `list(bool)` | `[]` | no |
| extra\_network\_interface\_private\_ips | List of lists containing private IPs to assign to the extra Network Interfaces for the EC2 instance. Each list must correspond to an extra Network Interface, in order. | `list(list(string))` | <pre>[<br/>  null<br/>]</pre> | no |
| extra\_network\_interface\_private\_ips\_counts | Number of secondary private IPs to assign to the ENI. The total number of private IPs will be 1 + private\_ips\_count, as a primary private IP will be assigned to an ENI by default. Make sure you have as many element in the list as ENIs times the number of instances. | `list(number)` | <pre>[<br/>  null<br/>]</pre> | no |
| extra\_network\_interface\_security\_group\_count | How many Security Groups to attach per extra Network Interface. Must be the number of element of `var.extra_network_interface_security_group_ids`. This cannot be computed automatically in Terraform 0.13. | `number` | `0` | no |
| extra\_network\_interface\_security\_group\_ids | List of Security Group IDs to assign to the extra Network Interfaces for the EC2 instance. All extra Network Interfaces will have the same Security Groups. If not specified, all ENI will have the `default` Security Group of the VPC. | `list(string)` | `null` | no |
| extra\_network\_interface\_source\_dest\_checks | Whether to enable source destination checking for the extra Network Interfaces for the EC2 instance. Default to `true`. | `list(bool)` | <pre>[<br/>  null<br/>]</pre> | no |
| extra\_network\_interface\_tags | Tags for the extra Network Interfaces for the EC2 instance. Will be merged with `var.tags`. These tags will be shared among all extra ENIs. | `map(string)` | `{}` | no |
| extra\_volumes | Customize extra-volumes on the EC2 instance (or launch template):<br/>  * name (optional, string): Name (tag:Name) of the extra volume to create. Will be suffixed by numerical digits if `var.use_num_suffix` is `true`. Default to `vol`.<br/>  * device\_name (required, string): Device name for the extra volume to attached to the EC2 instance (or the launch template).<br/>  * type (optional, string): The volume type of the extra volume to attach to the EC2 instance (or launch template). Can be `standard`, `gp2`, `io1`, `sc1` or `st1` (Default: `gp3`).<br/>  * size (required, number): Size of the extra volumes for the EC2 instance (or launch template).<br/>  * throughput (optional, number): The throughput that the volume supports, in MiB/s. Only valid for type of `gp3`.<br/>  * iops (optional, number): The amount of IOPS to provision for the disk. Only valid for type of `io1`, `io2` or `gp3`.<br/>  * final\_snapshot\_enabled (optional, bool):  If true, snapshot will be created before volume deletion. Any tags on the volume will be migrated to the snapshot. By default set to false.<br/>  * multi\_attach\_enabled (optional, bool): Specifies whether to enable Amazon EBS Multi-Attach. Multi-Attach is supported on `io1` and `io2` volumes.<br/>  * snapshot\_id (optional, string): A snapshot to base the EBS volume off of. | <pre>map(object({<br/>    name                   = optional(string, "vol")<br/>    device_name            = string<br/>    type                   = optional(string, "gp3")<br/>    size                   = number<br/>    throughput             = optional(number)<br/>    iops                   = optional(number)<br/>    final_snapshot_enabled = optional(bool)<br/>    multi_attach_enabled   = optional(bool)<br/>    snapshot_id            = optional(string)<br/>  }))</pre> | `{}` | no |
| host\_id | The Id of a dedicated host that the instance will be assigned to. Use when an instance (or launch template) is to be launched on a specific dedicated host. | `string` | `null` | no |
| iam\_instance\_profile\_create | Whether to create an Instance Profile (with its IAM Role) for the EC2 instance (or launch template). If `false`, you can use `var.iam_instance_profile_name` to use an external IAM Instance Profile. | `bool` | `false` | no |
| iam\_instance\_profile\_iam\_instance\_profile\_tags | Tags to be used for the Instance Profile. Will be merged with `var.tags`. Ignored if `var.iam_instance_profile_create` is `false`. | `map(string)` | `{}` | no |
| iam\_instance\_profile\_iam\_role\_create | Whether to create the Instance Profile IAM Role if `var.iam_instance_profile_create` is set. | `bool` | `true` | no |
| iam\_instance\_profile\_iam\_role\_description | Description of the IAM Role to be used by the Instance Profile. Ignored if `var.iam_instance_profile_create` is `false`. | `string` | `"Instance Profile Role"` | no |
| iam\_instance\_profile\_iam\_role\_id | The ID of the IAM Instance Profile Role to attached to `var.iam_instance_profile_name` if `var.iam_instance_profile_create` is set and `var.iam_instance_profile_iam_role_create` is not set. If `var.iam_instance_profile_create` is set, `var.iam_instance_profile_iam_role_policy_arns` is not empty and `var.iam_instance_profile_iam_role_create` is not set, this module will attached `var.iam_instance_profile_iam_role_policy_arns` to the role passed in this variable. | `string` | `null` | no |
| iam\_instance\_profile\_iam\_role\_name | Name of the IAM Role to be used by the Instance Profile. If omitted, Terraform will assign a random, unique name. Ignored if `var.iam_instance_profile_create` is `false`. | `string` | `null` | no |
| iam\_instance\_profile\_iam\_role\_policy\_arns | ARNs of the IAM Policies to be applied to the IAM Role of the Instance Profile. Ignored if `var.iam_instance_profile_create` is `false`. | `map(string)` | `{}` | no |
| iam\_instance\_profile\_iam\_role\_tags | Tags to be used for the Instance Profile Role. Will be merged with `var.tags`. Ignored if `var.iam_instance_profile_create` is `false`. | `map(string)` | `{}` | no |
| iam\_instance\_profile\_name | The IAM profile's name for the EC2 instance (or launch template). If `var.iam_instance_profile_create` is `true` and this is null, Terraform will assign a random, unique name. If `var.iam_instance_profile_create` is `false` this value should be the name of an external IAM Instance Profile (keep it `null` to disable Instance Profile altogether). | `string` | `""` | no |
| iam\_instance\_profile\_path | Path in which to create the Instance Profile for the EC2 instance (or launch template). Instance Profile IAM Role will share the same path. Ignored if `var.iam_instance_profile_create` is `false`. | `string` | `"/"` | no |
| instance\_initiated\_shutdown\_behavior | Shutdown behavior for the EC2 instance (or launch template). Amazon defaults this to `stop` for EBS-backed instances and `terminate` for instance-store instances. Cannot be set on instance-store instances. | `string` | `null` | no |
| instance\_tags | Tags that will be shared with all the instances (or instances launched by the AutoScaling Group). Will be merged with `var.tags`. | `map(string)` | `{}` | no |
| instance\_type | The type of instance (or launch template) to start. Updates to this field will trigger a stop/start of the EC2 instance, except with launch template. | `string` | `"t3.nano"` | no |
| ipv4\_address\_count | A number of IPv4 addresses to associate with the primary network interface of the EC2 instance (or launch template). The total number of private IPs will be 1 + `var.ipv4_address_count`, as a primary private IP will be assigned to an ENI by default. | `number` | `0` | no |
| key\_pair\_create | Whether to create a key pair. If `true`, please provide a `var.key_pair_public_key`. If `false`, use `var.key_pair_name` to inject an external key pair. If `var.key_pair_name` is `null`, no key pair will be added. | `bool` | `false` | no |
| key\_pair\_name | The name for the key pair. If this is not empty and `var.key_pair_create` = `false`, this name will be used as an external key pair. If you don't want any key pair, set this to `null`. | `string` | `null` | no |
| key\_pair\_public\_key | The public key material. Ignored if `var.key_pair_create` is `false`. | `string` | `null` | no |
| key\_pair\_tags | Tags specific for the key pair. Will be merged with `var.tags`. Ignored if `var.key_pair_create` is `false`. | `map(string)` | `{}` | no |
| launch\_template\_ipv6\_address\_count | A number of IPv6 addresses to associate with the primary network interface of the launch template. | `number` | `0` | no |
| launch\_template\_name | The name of the launch template. If you leave this blank, Terraform will auto-generate a unique name. | `string` | `""` | no |
| launch\_template\_tags | Tags to be used by the launch template. Will be merge with var.tags. | `map(string)` | `{}` | no |
| metadata\_options\_http\_endpoint\_enabled | Whether to enable the HTTP metadata endpoint | `bool` | `true` | no |
| metadata\_options\_http\_put\_response\_hop\_limit | The desired HTTP PUT response hop limit for instance metadata requests. The larger the number, the further instance metadata requests can travel. Can be an integer from 1 to 64. Default to 1. It will be ignore if `var.metadata_options_http_endpoint_enabled` is set and `var.use_autoscaling_group` is not set | `number` | `1` | no |
| metadata\_options\_http\_tokens\_required | Whether the metadata service requires session tokens, also referred to as Instance Metadata Service Version 2 (IMDSv2). It will be ignore if `var.metadata_options_http_endpoint_enabled` is set and `var.use_autoscaling_group` is not set | `bool` | `true` | no |
| metadata\_options\_instance\_metadata\_tags\_enabled | Enables or disables access to instance tags from the instance metadata service. It will be ignore if `var.metadata_options_http_endpoint_enabled` is set and `var.use_autoscaling_group` is not set | `bool` | `true` | no |
| monitoring | If `true`, the launched EC2 instance (or launch template) will have detailed monitoring enabled: 1 minute granularity instead of 5 minutes. Incurs additional costs. | `bool` | `false` | no |
| name | Name (tag:Name) of the instance(s). | `string` | `"ec2"` | no |
| network\_interface\_tags | Tags of the primary Network Interface of the instance its extra nic; or  the launch template network interfaces in case of an AutoScaling Group. Will be merged with `var.tags`. | `map(string)` | `{}` | no |
| placement\_group | ID of the Placement Group to start the EC2 instance (or launch template) in. | `string` | `null` | no |
| prefix | Prefix to be added to with all resource's names of the module. Prefix is mainly used for tests and should remain empty in normal circumstances. | `string` | `""` | no |
| root\_block\_device\_delete\_on\_termination | Whether to delete the root block device on termination. **It's is strongly discouraged** to set this to `false`: only change this value if you have no other choice as this will leave a volume that will not be managed by terraform (even if the tag says it does) and you may end up building up costs. | `bool` | `true` | no |
| root\_block\_device\_encrypted | Customize details about the root block device of the EC2 instance (or launch template) root volume: enables EBS encryption on the volume. Cannot be used with snapshot\_id. Must be configured to perform drift detection. | `bool` | `true` | no |
| root\_block\_device\_iops | The amount of provisioned IOPS. This must be set when `var.root_block_device_volume_type` is `io1`, `io2` or `gp3`. | `number` | `null` | no |
| root\_block\_device\_throughput | Throughput to provision for a volume in mebibytes per second (MiB/s). This is only valid for volume\_type of `gp3`. | `number` | `null` | no |
| root\_block\_device\_volume\_device | Device name of the root volume of the AMI. Only used for Launch Template. This value cannot be found by the AWS Terraform provider from the AMI ID alone. If this value is wrong, Terraform will create an extra volume, failing to setup root volume correctly. Can be `/dev/sda1` or `/dev/xdva`. | `string` | `"/dev/xvda"` | no |
| root\_block\_device\_volume\_size | Customize details about the root block device of the instance or launch template root volume: The size of the volume in gibibytes (GiB). | `number` | `8` | no |
| root\_block\_device\_volume\_type | Customize details about the root block device of the instance or launch template root volume: The type of volume. Can be `standard`, `gp2`, `gp3`, `io1`, `sc1` or `st1`. | `string` | `"gp3"` | no |
| tags | Tags to be used for all this module resources. Will be merged with specific tags for each kind of resource. | `map(string)` | `{}` | no |
| tenancy | The tenancy of the EC2 instance (if the instance or launch template will be running in a VPC). An instance with a tenancy of `dedicated` runs on single-tenant hardware. The `host` tenancy is not supported for the import-instance command. | `string` | `null` | no |
| use\_autoscaling\_group | Whether to create an AutoScaling Group instead of an EC2 instance. If `true`, use `autoscaling_group`-prefixed variables. | `bool` | `false` | no |
| use\_default\_subnet | Whether to use the default subnet instead of `ec2_subnet_id` or `autoscaling_group_subnet_ids`. If `null`, the module will try its best to determine if default subnet should be used or not but sometimes Terraform fails with this error: “The count value depends on resource attributes that cannot be determined until apply”. If it is the case, set this value to `true` or `false` to force the use of default subnets or not. | `bool` | `null` | no |
| user\_data | The user data to provide when launching the EC2 instance (or launch template). | `string` | `null` | no |
| volume\_kms\_key\_alias | Alias of the KMS key used to encrypt the root and extra volumes of the EC2 instance (or launch template). Do not prefix this value with `alias/` nor with a `/`. | `string` | `"default/ec2"` | no |
| volume\_kms\_key\_arn | ARN of an external KMS key used to encrypt the root and extra volumes. Can be used only if `var.volume_kms_key_create` is set to `false`. | `string` | `""` | no |
| volume\_kms\_key\_create | Whether to create a KMS key to be used for root and extra volumes. If set to `false`, a `var.volume_kms_key_arn` can be specified as an external KMS key instead. If this value is `false` and `var.volume_kms_key_arn` empty, the default AWS KMS key for volumes will be used. | `bool` | `false` | no |
| volume\_kms\_key\_customer\_master\_key\_spec | Specifies whether the key contains a symmetric key or an asymmetric key pair and the encryption algorithms or signing algorithms that the key supports for the KMS key to be used for volumes. Valid values: `SYMMETRIC_DEFAULT`, `RSA_2048`, `RSA_3072`, `RSA_4096`, `ECC_NIST_P256`, `ECC_NIST_P384`, `ECC_NIST_P521`, or `ECC_SECG_P256K1`. Defaults to `SYMMETRIC_DEFAULT`. | `string` | `null` | no |
| volume\_kms\_key\_name | Name (tag:Name) for the KMS key to be used for root and extra volumes of the EC2 instance (or launch template). | `string` | `"kms-for-vol"` | no |
| volume\_kms\_key\_policy | A valid policy JSON document for the KMS key to be used for root and extra volumes of the EC2 instance (or launch template). This document can give or restrict accesses for the key. | `string` | `null` | no |
| volume\_kms\_key\_tags | Tags for the KMS key to be used for root and extra volumes. Will be merge with `var.tags`. | `map(string)` | `{}` | no |
| volume\_tags | Tags of the root volume of the instance and its extra volumes; or the launch template volumes in case of an AutoScaling Group. Will be merged with `var.tags`. | `map(string)` | `{}` | no |
| vpc\_security\_group\_ids | List of security group IDs to associate with the main ENI of the EC2 instance (or launch template). If not defined, the default VPC security group will be used. | `list(string)` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| aws\_autoscaling\_group | n/a |
| aws\_autoscaling\_schedules | n/a |
| aws\_ebs\_volumes | n/a |
| aws\_eips | n/a |
| aws\_iam\_instance\_profile | n/a |
| aws\_iam\_role | n/a |
| aws\_iam\_service\_linked\_role | n/a |
| aws\_instance | n/a |
| aws\_key\_pair | n/a |
| aws\_kms\_key | n/a |
| aws\_launch\_template | n/a |
| aws\_network\_interface | n/a |
| aws\_network\_interfaces | n/a |
| precomputed | n/a |
<!-- END_TF_DOCS -->
