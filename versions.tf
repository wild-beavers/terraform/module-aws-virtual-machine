terraform {
  required_version = ">= 1.8"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5"
    }
    null = {
      source  = "hashicorp/null"
      version = ">= 2.1"
    }
    time = {
      source  = "hashicorp/time"
      version = ">= 0"
    }
  }
}
