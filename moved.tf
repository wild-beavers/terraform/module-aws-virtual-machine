# TO BE SAFELY REMOVED WHEN 19.0.0 appears in CHANGELOG

moved {
  from = aws_launch_template.this[0]
  to   = aws_launch_template.this["0"]
}

moved {
  from = aws_autoscaling_group.this[0]
  to   = aws_autoscaling_group.this["0"]
}

moved {
  from = aws_iam_service_linked_role.asg[0]
  to   = aws_iam_service_linked_role.asg["0"]
}

moved {
  from = aws_instance.this[0]
  to   = aws_instance.this["0"]
}

moved {
  from = aws_instance.this_ignore_changes[0]
  to   = aws_instance.this_ignore_ami_user_data_changes["0"]
}

moved {
  from = aws_network_interface.this_primary[0]
  to   = aws_network_interface.this_primary["0"]
}

moved {
  from = aws_iam_instance_profile.this[0]
  to   = aws_iam_instance_profile.this["0"]
}

moved {
  from = aws_iam_role.this_instance_profile[0]
  to   = aws_iam_role.this_instance_profile["0"]
}

moved {
  from = aws_eip.this_primary[0]
  to   = aws_eip.this_primary["0"]
}

moved {
  from = aws_eip_association.this_primary[0]
  to   = aws_eip_association.this_primary["0"]
}

moved {
  from = aws_key_pair.this[0]
  to   = aws_key_pair.this["0"]
}

moved {
  from = aws_kms_key.this_volume[0]
  to   = aws_kms_key.this_volume["0"]
}

moved {
  from = aws_kms_alias.this_volume[0]
  to   = aws_kms_alias.this_volume["0"]
}

moved {
  from = aws_kms_grant.this_volume[0]
  to   = aws_kms_grant.this_volume["0"]
}

# END TO BE SAFELY REMOVED WHEN 19.0.0 appears in CHANGELOG
