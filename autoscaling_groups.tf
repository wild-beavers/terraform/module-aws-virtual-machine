####
# Launch Template
####

resource "aws_launch_template" "this" {
  for_each = var.use_autoscaling_group ? { 0 = "enabled" } : {}

  name = format("%s%s", var.prefix, var.launch_template_name)

  image_id      = local.ami
  instance_type = var.instance_type
  key_name      = local.key_pair_name

  user_data = var.user_data

  disable_api_termination = var.disable_api_termination

  ebs_optimized = var.ebs_optimized

  tags = merge(
    local.tags,
    {
      Name = format("%s%s", var.prefix, var.launch_template_name)
    },
    var.launch_template_tags,
  )

  dynamic "cpu_options" {
    for_each = (var.cpu_threads_per_core != null || var.cpu_core_count != null) ? [1] : []

    content {
      core_count       = var.cpu_core_count
      threads_per_core = var.cpu_threads_per_core
    }
  }

  dynamic "credit_specification" {
    for_each = local.is_t_instance_type && var.cpu_credits != "" ? [1] : []

    content {
      cpu_credits = var.cpu_credits
    }
  }

  dynamic "block_device_mappings" {
    for_each = local.should_update_root_device ? [1] : []

    content {
      device_name = var.root_block_device_volume_device

      ebs {
        delete_on_termination = true
        encrypted             = var.root_block_device_encrypted
        iops                  = var.root_block_device_iops
        volume_size           = var.root_block_device_volume_size
        volume_type           = var.root_block_device_volume_type
        kms_key_id            = local.volume_kms_key_arn
      }
    }
  }

  dynamic "block_device_mappings" {
    for_each = var.extra_volumes

    content {
      device_name = block_device_mappings.value.device_name

      ebs {
        delete_on_termination = true
        encrypted             = true
        volume_size           = block_device_mappings.value.size
        volume_type           = block_device_mappings.value.type
        kms_key_id            = local.volume_kms_key_arn
      }
    }
  }

  dynamic "block_device_mappings" {
    for_each = var.ephemeral_block_devices

    content {
      device_name  = block_device_mappings.value.device_name
      virtual_name = lookup(block_device_mappings.value, "virtual_name", null)
      no_device    = lookup(block_device_mappings.value, "no_device", null)
    }
  }

  dynamic "iam_instance_profile" {
    for_each = local.iam_instance_profile_name != "" ? [1] : []

    content {
      name = local.iam_instance_profile_name
    }
  }

  dynamic "monitoring" {
    for_each = var.monitoring == true ? [1] : []

    content {
      enabled = true
    }
  }

  network_interfaces {
    description = format("%s%s", var.prefix, "root network interface for ${var.name}")

    security_groups             = local.security_group_ids
    associate_public_ip_address = var.associate_public_ip_address
    ipv6_address_count          = var.launch_template_ipv6_address_count
    ipv4_address_count          = var.ipv4_address_count
    delete_on_termination       = true
  }

  dynamic "placement" {
    for_each = var.placement_group != null ? [1] : []

    content {
      availability_zone = local.availability_zones[0]
      group_name        = var.placement_group
      tenancy           = var.tenancy
      host_id           = var.host_id
    }
  }

  dynamic "metadata_options" {
    for_each = var.metadata_options_http_endpoint_enabled ? [1] : []

    content {
      http_endpoint               = "enabled"
      http_tokens                 = var.metadata_options_http_tokens_required == true ? "required" : "optional"
      http_put_response_hop_limit = var.metadata_options_http_put_response_hop_limit
      instance_metadata_tags      = var.metadata_options_instance_metadata_tags_enabled == true ? "enabled" : "disabled"
    }
  }

  tag_specifications {
    resource_type = "instance"

    tags = merge(
      local.tags,
      {
        managed-by = "ASG"
      },
      var.instance_tags,
    )
  }

  tag_specifications {
    resource_type = "volume"

    tags = merge(
      local.tags,
      var.volume_tags,
    )
  }

  tag_specifications {
    resource_type = "network-interface"

    tags = merge(
      local.tags,
      var.network_interface_tags,
    )
  }
}

####
# AutoScaling Group
####

resource "aws_iam_service_linked_role" "asg" {
  for_each = var.use_autoscaling_group ? { 0 = "enabled" } : {}

  aws_service_name = "autoscaling.amazonaws.com"
  // IAM Roles cannot exceed 64 chars. Prefix uses 29 characters. Thus, ash name is truncated.
  custom_suffix = substr(format("%s%s", var.prefix, var.autoscaling_group_name), 0, 35)
  tags          = merge(local.tags)
}

// For some reason, using the "aws_iam_service_linked_role" produce inconsistent result
// Due to AWS answering resource is ready before it actually is.
// While wildbeavers consider it a bad practices,
// the explicit dependency on time waiting "wait_service_linked_role_asg" is the officially recommended fix
resource "time_sleep" "wait_service_linked_role_asg" {
  create_duration = "10s"

  depends_on = [aws_iam_service_linked_role.asg]
}

####
# AutoScaling Group
####

resource "aws_autoscaling_group" "this" {
  for_each = var.use_autoscaling_group ? { 0 = "enabled" } : {}

  name = format("%s%s", var.prefix, var.autoscaling_group_name)

  desired_capacity = var.autoscaling_group_desired_capacity
  max_size         = var.autoscaling_group_max_size
  min_size         = var.autoscaling_group_min_size

  health_check_grace_period = var.autoscaling_group_health_check_grace_period == -1 ? null : var.autoscaling_group_health_check_grace_period
  health_check_type         = var.autoscaling_group_health_check_type
  default_cooldown          = var.autoscaling_group_default_cooldown == -1 ? null : var.autoscaling_group_default_cooldown

  force_delete              = false
  wait_for_capacity_timeout = var.autoscaling_group_wait_for_capacity_timeout
  min_elb_capacity          = var.autoscaling_group_min_elb_capacity
  wait_for_elb_capacity     = var.autoscaling_group_wait_for_elb_capacity

  vpc_zone_identifier = local.subnet_ids

  dynamic "instance_maintenance_policy" {
    for_each = var.autoscaling_group_maintenance_policy != null ? [1] : []

    content {
      max_healthy_percentage = var.autoscaling_group_maintenance_policy.max_healthy_percentage
      min_healthy_percentage = var.autoscaling_group_maintenance_policy.min_healthy_percentage
    }
  }

  launch_template {
    id      = aws_launch_template.this[0].id
    version = aws_launch_template.this[0].latest_version
  }

  capacity_rebalance    = var.autoscaling_group_capacity_rebalance
  termination_policies  = var.autoscaling_group_termination_policies
  suspended_processes   = var.autoscaling_group_suspended_processes
  metrics_granularity   = var.autoscaling_group_metrics_granularity
  enabled_metrics       = var.autoscaling_group_enabled_metrics
  max_instance_lifetime = var.autoscaling_group_max_instance_lifetime
  protect_from_scale_in = var.autoscaling_group_protect_from_scale_in

  placement_group = var.placement_group

  service_linked_role_arn = aws_iam_service_linked_role.asg["0"].arn

  dynamic "instance_refresh" {
    for_each = var.autoscaling_group_instance_refresh_enabled == true ? [1] : []

    content {
      strategy = var.autoscaling_group_instance_refresh_strategy
      preferences {
        min_healthy_percentage = var.autoscaling_group_instance_refresh_min_healthy_percentage
        standby_instances      = var.autoscaling_group_instance_refresh_standby_instances
      }
      triggers = var.autoscaling_group_instance_refresh_triggers
    }
  }

  dynamic "tag" {
    for_each = merge(local.tags, var.autoscaling_group_tags)

    content {
      key                 = tag.key
      value               = tag.value != null ? tag.value : ""
      propagate_at_launch = false
    }
  }

  timeouts {
    delete = "15m"
  }

  lifecycle {
    ignore_changes = [target_group_arns]
  }

  depends_on = [
    time_sleep.wait_service_linked_role_asg
  ]
}

resource "aws_autoscaling_attachment" "this" {
  count = var.use_autoscaling_group ? length(var.autoscaling_group_target_group_arns) : 0

  autoscaling_group_name = aws_autoscaling_group.this["0"].id
  lb_target_group_arn    = element(var.autoscaling_group_target_group_arns, count.index)
}

resource "aws_autoscaling_schedule" "this" {
  for_each = var.use_autoscaling_group ? var.autoscaling_schedules : {}

  scheduled_action_name = format("%s%s", var.prefix, each.value.name)
  min_size              = each.value.min_size
  max_size              = each.value.max_size
  desired_capacity      = each.value.desired_capacity
  recurrence            = each.value.recurrence
  start_time            = each.value.start_time != null ? each.value.start_time : timeadd(timestamp(), "1m")
  end_time              = each.value.end_time

  autoscaling_group_name = aws_autoscaling_group.this["0"].name

  lifecycle {
    ignore_changes = [
      start_time,
    ]
  }
}

####
# AutoScaling Group Notification
####

resource "aws_autoscaling_notification" "this" {
  for_each = var.use_autoscaling_group ? var.autoscaling_notifications : {}

  group_names = [aws_autoscaling_group.this["0"].name]

  notifications = each.value.notifications
  topic_arn     = each.value.topic_arn
}
