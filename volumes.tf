####
# Extra EBS
####

locals {
  should_create_extra_volumes = local.extra_volume_count > 0 && var.use_autoscaling_group == false
}

resource "aws_volume_attachment" "this_extra" {
  for_each = local.should_create_extra_volumes ? var.extra_volumes : {}

  device_name = each.value.device_name
  volume_id   = aws_ebs_volume.this_extra[each.key].id
  instance_id = var.ec2_ignore_change_on_ami_and_user_data ? aws_instance.this_ignore_ami_user_data_changes[0].id : aws_instance.this[0].id
}

resource "aws_ebs_volume" "this_extra" {
  for_each = local.should_create_extra_volumes ? var.extra_volumes : {}

  availability_zone    = local.availability_zones[0]
  size                 = each.value.size
  type                 = each.value.type
  final_snapshot       = each.value.final_snapshot_enabled
  iops                 = each.value.iops
  multi_attach_enabled = each.value.multi_attach_enabled
  snapshot_id          = each.value.snapshot_id
  throughput           = each.value.throughput

  encrypted  = true
  kms_key_id = local.volume_kms_key_arn
  tags = merge(
    local.tags,
    {
      Name = format("%s%s", var.prefix, each.value.name)
    },
    var.volume_tags,
  )
}
